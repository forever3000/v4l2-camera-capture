// Martin Kersner, m.kersner@gmail.com
// 2017/03/08

#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <string.h>
#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <linux/videodev2.h>

#include <iostream>
#include <string>
#include <iostream>

#include "CameraUtils.hpp"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

struct buffer {
  void   *start;
  size_t  length;
};

class Camera {
  public:
    Camera();
    Camera(const std::string &);
    ~Camera();

    std::string   devName;
    int           fd;
    int           forceFormat;
    struct buffer *buffers;
    unsigned int  nBuffers;

    int GetWidth();
    int GetHeight();

  private:
    int width;
    int height;
    int bufferSize;

    void OpenDevice();
    void InitDevice();
    void StartCapturing();
    void StopCapturing();
    void UninitDevice();
    void CloseDevice();
    void InitMmap();
};

#endif // CAMERA_HPP_
