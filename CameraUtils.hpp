// Martin Kersner, m.kersner@gmail.com
// 2017/03/08

#ifndef CAMERAUTILS_HPP_
#define CAMERAUTILS_HPP_

#include <string.h>
#include <iostream>
#include <errno.h>
#include <sys/ioctl.h>

int xioctl(int, int, void *);
void errno_exit(const std::string &);

#endif // CAMERAUTILS_HPP_
