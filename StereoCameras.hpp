// Martin Kersner, m.kersner@gmail.com
// 2017/03/08

#ifndef STEROCAMERAS_HPP_
#define STEROCAMERAS_HPP_

#include <errno.h>
#include <assert.h>
#include <sys/time.h>
#include <linux/videodev2.h>

#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>
#include "opencv2/opencv.hpp"

#include "Camera.hpp"
#include "CameraUtils.hpp"

struct StereoFrames {
  cv::Mat left;
};

class StereoCameras {
  public:
    StereoCameras();
    StereoCameras(const std::string &);
    ~StereoCameras();
    int CaptureFrames();
    bool GetFrames(cv::Mat &);
    void GetReady();
    int GetFrameHeight() { return this->frameHeight; }
    int GetFrameWidth() { return this->frameWidth; }
    void ThreadRelease();

  private:
    Camera * cameraLeft;
    int frameWidth;
    int frameHeight;
    StereoFrames stereoFrames;
    std::thread cameraThread;
    std::mutex dataMutex;
    std::mutex isRunningMutex;

    bool isRunning = true;

    void Initialization();
    void Loop();
    int CaptureFrames(int);
    void ConvertFrames(struct v4l2_buffer &);
};

#endif // STEROCAMERAS_HPP_
