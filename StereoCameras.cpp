// Martin Kersner, m.kersner@gmail.com
// 2017/03/08
// 
// Capture frames from two cameras simultaneously.
// Based on https://linuxtv.org/downloads/v4l-dvb-apis/uapi/v4l/capture.c.html
//
// Minimal example:

#include "opencv2/opencv.hpp"
#include "StereoCameras.hpp"

int main() {
  StereoCameras sc;
  cv::Mat left;

  sc.GetReady();

  while(1) {
    sc.GetFrames(left);
    cv::imshow("left", left);
    cv::waitKey(1);
  }

  return 1;
}

StereoCameras::StereoCameras() {
  this->cameraLeft  = new Camera("/dev/video0");

  this->Initialization();
}

StereoCameras::StereoCameras(const std::string & cameraLeftName) {

  this->cameraLeft  = new Camera(cameraLeftName);
  this->Initialization();
}

StereoCameras::~StereoCameras() {
  delete this->cameraLeft;
}

void StereoCameras::Initialization() {
  // it is expected that cameras capture in the same resolution
  this->frameWidth  = this->cameraLeft->GetWidth();
  this->frameHeight = this->cameraLeft->GetHeight();
 
  this->cameraThread = std::thread(&StereoCameras::Loop, this);
}

int StereoCameras::CaptureFrames(int num) {
  struct v4l2_buffer bufLeft;
  unsigned int i;

  CLEAR(bufLeft);

  bufLeft.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  bufLeft.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(this->cameraLeft->fd,  VIDIOC_DQBUF, &bufLeft)) {
    switch (errno) {
      case EAGAIN:
        return 0;

      case EIO:
        //[> Could ignore EIO, see spec. <]

        //[> fall through <]

      default:
        errno_exit("VIDIOC_DQBUF");
    }
  }

  assert(bufLeft.index  < this->cameraLeft->nBuffers);

  if ((-1 == xioctl(this->cameraLeft->fd,  VIDIOC_QBUF, &bufLeft)))
    errno_exit("VIDIOC_QBUF");

  // cameras capture frames at 60 Hz, but we want to capture frames only at 30 Hz
  if ((num % 2) == 0)
    return 1;
  else
    this->ConvertFrames(bufLeft);

  return 1;
}

void StereoCameras::Loop() {
  unsigned int num = 0;

  while (1) {
    { std::lock_guard<std::mutex> lock(this->isRunningMutex);
      if (!this->isRunning)
        break;
    }

    for (;;) {
      num++;
      fd_set fdsLeft;
      struct timeval tvLeft;
      int rLeft;

      FD_ZERO(&fdsLeft);
      FD_SET(this->cameraLeft->fd, &fdsLeft);

      //[> Timeout. <]
      tvLeft.tv_sec = 2;
      tvLeft.tv_usec = 0;

      rLeft  = select(this->cameraLeft->fd + 1,  &fdsLeft,  NULL, NULL, &tvLeft);

      if (-1 == rLeft) {
        if (EINTR == errno)
          continue;
        errno_exit("select");
      }

      if (0 == rLeft) {
        std::cerr << "select timeout\n" << std::endl;
        exit(EXIT_FAILURE);
      }

      if (this->CaptureFrames(num))
        break;
      //[> EAGAIN - continue select loop. <]
    }
  }
}

void StereoCameras::ConvertFrames(struct v4l2_buffer & bufLeft) {
  cv::Mat grayLeft;

  cv::Mat inputLeft(this->frameHeight, this->frameWidth, CV_8UC2);
  memcpy(inputLeft.data, (unsigned char *) this->cameraLeft->buffers[bufLeft.index].start, this->cameraLeft->buffers[bufLeft.index].length * sizeof(unsigned char));
  cv::cvtColor(inputLeft, grayLeft, CV_YUV2BGR_UYVY);

  // copy frames to safe place
  std::lock_guard<std::mutex> lock(this->dataMutex);
  this->stereoFrames.left  = grayLeft;
}

bool StereoCameras::GetFrames(cv::Mat & left) {
  std::lock_guard<std::mutex> lock(this->dataMutex);
  left  = this->stereoFrames.left;

  return true;
}

// Waiting until cameras start capturing frames.
void StereoCameras::GetReady() {
  cv::Mat left;

  while (1) {
    this->GetFrames(left);
    if (left.rows != 0)
      break;
  }
}

void StereoCameras::ThreadRelease() {
  { std::lock_guard<std::mutex> lock(this->isRunningMutex);
    this->isRunning = false;
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  this->cameraThread.join();
}
