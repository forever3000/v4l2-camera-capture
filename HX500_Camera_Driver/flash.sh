#!/bin/bash
sudo cp -v ./Image /boot/
sudo cp -v ./*.dtb /boot/
#Use only for 27.1 kernel, on TX2 side
#sudo sed -i '8 a \ \ \ \ \ \ FDT /boot/tegra186-quill-p3310-1000-c03-00-base.dtb' /boot/extlinux/extlinux.conf
#Use for 28.1 kernel, on PC side
#sudo ./flash.sh -r -k kernel-dtb jetson-tx2 mmcblk0p1
sync
