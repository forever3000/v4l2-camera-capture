// Martin Kersner, m.kersner@gmail.com
// 2017/03/08
//
// Initialize and deinitialize camera for capturing.
// Based on https://linuxtv.org/downloads/v4l-dvb-apis/uapi/v4l/capture.c.html
//
// Minimal example:
//
// Camera cam0("dev/video0");

#include "Camera.hpp"

Camera::Camera(const std::string & devName_) 
  : devName(devName_),
    fd(-1),
    forceFormat(1),
    width(1280),
    height(720),
    bufferSize(8)
{
  this->OpenDevice();
  this->InitDevice();
  this->StartCapturing();
};

Camera::Camera()
  : fd(-1),
    nBuffers(0),
    buffers(NULL)
{}

Camera::~Camera() {
  if (this->fd != -1)  {
    this->StopCapturing();
    this->UninitDevice();
    this->CloseDevice();
  }
}

void Camera::OpenDevice() {
  struct stat st;

  if (-1 == stat(this->devName.c_str(), &st)) {
    std::cerr << "Cannot identify " << this->devName.c_str() << ": " << errno << ", " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }

  if (!S_ISCHR(st.st_mode)) {
    std::cerr << this->devName.c_str() << " is not device" << std::endl;
    exit(EXIT_FAILURE);
  }

  this->fd = open(this->devName.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

  if (-1 == this->fd) {
    std::cerr << "Cannot open " << this->devName.c_str() << ": " << errno << ", " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }
}

void Camera::InitDevice() {
  struct v4l2_capability cap;
  struct v4l2_cropcap cropcap;
  struct v4l2_crop crop;
  struct v4l2_format fmt;
  unsigned int min;

  if (-1 == xioctl(this->fd, VIDIOC_QUERYCAP, &cap)) {
    if (EINVAL == errno) {
      std::cerr << this->devName.c_str() << " is not V4L2 device" << std::endl;
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_QUERYCAP");
    }
  }

  if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
    std::cerr << this->devName.c_str() << " is not video capture device" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
    std::cerr << this->devName.c_str() << " does not support streaming i/o" << std::endl;
    exit(EXIT_FAILURE);
  }

  /* Select video input, video standard and tune here. */

  CLEAR(cropcap);

  cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (0 == xioctl(this->fd, VIDIOC_CROPCAP, &cropcap)) {
    crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    crop.c = cropcap.defrect; /* reset to default */

    if (-1 == xioctl(this->fd, VIDIOC_S_CROP, &crop)) {
      switch (errno) {
        case EINVAL:
          /* Cropping not supported. */
          break;
        default:
          /* Errors ignored. */
          break;
      }
    }
  } else {
    /* Errors ignored. */
  }

  CLEAR(fmt);

  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (this->forceFormat) {
    fmt.fmt.pix.width       = 1280;
    fmt.fmt.pix.height      = 720;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
    fmt.fmt.pix.field       = V4L2_FIELD_NONE;

    if (-1 == xioctl(this->fd, VIDIOC_S_FMT, &fmt))
      errno_exit("VIDIOC_S_FMT");

    /* Note VIDIOC_S_FMT may change width and height. */
  } else {
    /* Preserve original settings as set by v4l2-ctl for example */
    if (-1 == xioctl(this->fd, VIDIOC_G_FMT, &fmt))
      errno_exit("VIDIOC_G_FMT");
  }

  /* Buggy driver paranoia. */
  min = fmt.fmt.pix.width * 2;
  if (fmt.fmt.pix.bytesperline < min)
    fmt.fmt.pix.bytesperline = min;
  min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
  if (fmt.fmt.pix.sizeimage < min)
    fmt.fmt.pix.sizeimage = min;

  this->InitMmap();
}

void Camera::InitMmap() {
  struct v4l2_requestbuffers req;

  CLEAR(req);

  req.count = this->bufferSize;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(this->fd, VIDIOC_REQBUFS, &req)) {
    if (EINVAL == errno) {
      std::cerr << this->devName.c_str() << " does not support memory mapping" << std::endl;
      exit(EXIT_FAILURE);
    } else {
      errno_exit("VIDIOC_REQBUFS");
    }
  }

  if (req.count < 2) {
    std::cerr << "Insufficient buffer memory on " << this->devName.c_str() << std::endl;
    exit(EXIT_FAILURE);
  }

  this->buffers = (buffer *)calloc(req.count, sizeof(*this->buffers));

  if (!this->buffers) {
    std::cerr << "Out ouf memory" << std::endl;
    exit(EXIT_FAILURE);
  }

  for (this->nBuffers = 0; this->nBuffers < req.count; ++this->nBuffers) {
    struct v4l2_buffer buf;

    CLEAR(buf);

    buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory      = V4L2_MEMORY_MMAP;
    buf.index       = this->nBuffers;

    if (-1 == xioctl(this->fd, VIDIOC_QUERYBUF, &buf))
      errno_exit("VIDIOC_QUERYBUF");

    this->buffers[this->nBuffers].length = buf.length;
    this->buffers[this->nBuffers].start =
      mmap(NULL /* start anywhere */,
          buf.length,
          PROT_READ | PROT_WRITE /* required */,
          MAP_SHARED /* recommended */,
          this->fd, buf.m.offset);

    if (MAP_FAILED == this->buffers[this->nBuffers].start)
      errno_exit("mmap");
  }
}

void Camera::StartCapturing() {
  unsigned int i;
  enum v4l2_buf_type type;

  for (i = 0; i < this->nBuffers; ++i) {
    struct v4l2_buffer buf;

    CLEAR(buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.index = i;

    if (-1 == xioctl(this->fd, VIDIOC_QBUF, &buf))
      errno_exit("VIDIOC_QBUF");
  }

  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (-1 == xioctl(this->fd, VIDIOC_STREAMON, &type))
    errno_exit("VIDIOC_STREAMON");

}

void Camera::StopCapturing() {
  enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (-1 == xioctl(this->fd, VIDIOC_STREAMOFF, &type))
    errno_exit("VIDIOC_STREAMOFF");
}

void Camera::UninitDevice() {
  unsigned int i;

  for (i = 0; i < this->nBuffers; ++i)
    if (-1 == munmap(this->buffers[i].start, this->buffers[i].length))
      errno_exit("munmap");

  free(this->buffers);
}

void Camera::CloseDevice() {
  if (-1 == close(this->fd))
    errno_exit("close");

  this->fd = -1;
}

int Camera::GetWidth() {
  return this->width;
}

int Camera::GetHeight() {
  return this->height;
}
