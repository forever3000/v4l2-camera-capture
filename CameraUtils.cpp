// Martin Kersner, m.kersner@gmail.com
// 2017/03/08

#include "CameraUtils.hpp"

int xioctl(int fh, int request, void *arg) {
  int r;

  do {
    r = ioctl(fh, request, arg);
  } while (-1 == r && EINTR == errno);

  return r;
}

void errno_exit(const std::string & s) {
  std::cerr << s << " error " << errno << ", " << strerror(errno) << std::endl;
  exit(EXIT_FAILURE);
}
